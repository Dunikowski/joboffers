<?php
require_once '../../../../wp-load.php';
require_once 'JobOfeers_Model.php';

class JobOffersAjax
{

    private static  $offset = 0;
    private static $limit = 10;
    private static $country = '';
    private static $model;
  
    public static function init($a = Null , $b = Null, $c = Null){

        self::$model = new JobOffers_Model();

        if($a != Null) {
           self::$country = $a;
        }

        if($b != Null) {
            self::$limit = $b;
        }

        if($c != Null) {
            self::$offset = $c;
        }
    }
  
   public static function displayItems(){

       if(self::$country != ''){

        $i = self::$model->getData(self::$country, self::$limit,self::$offset);

       } else {

        $i = self::$model->getAll(self::$limit,self::$offset);

       }

       echo json_encode($i);
   }
}

JobOffersAjax::init($_POST["country"],$_POST["limit"],$_POST["offset"]);
JobOffersAjax::displayItems();

