﻿<?php
require_once '../../../../wp-load.php';
require_once 'JobOfeers_Model.php';

class JobOffers_AutoUpdateTable {
    private static $url_source_xml = 'https://pl.europa.jobs/export/pracujwuniiorg.xml';
    private static $model;

    public static function init(){

        self::$model = new JobOffers_Model();
        libxml_use_internal_errors(true);
        $xml_file = self::getXML();

        if($xml_file !== false){

            self::$model->dropTable();
	        self::$model->createDBTable();
            self::$model->updateDB($xml_file);
        } else {
            $i = "Failed loading XML\n";
            foreach(libxml_get_errors() as $error) {
                $i .=  $error->message;
            }
            $to_email = 'd.dunikowski@webvist.pl';
            $headers = "Content-Type: text/html; charset=UTF-8";
            $subject = 'Błąd ładowania pliku xml';
            $message = $i;
            
            mail($to_email,$subject,$message,$headers);
            libxml_clear_errors();
        }
    }

    public static function getXML() {

        $i = simplexml_load_file(self::$url_source_xml,"SimpleXMLElement",LIBXML_NOCDATA);
        
        return $i;
    }
}

JobOffers_AutoUpdateTable::init();