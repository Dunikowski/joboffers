<?php 

class JobOffers_Model{
    private $table_name = 'job_offers';
    private $wpdb;

    function __construct(){
        global $wpdb;
        $this->wpdb = $wpdb;
    }

    public function getTableName(){

        return $this->wpdb->prefix.$this->table_name;
    }

   public function createDBTable(){
       
        $sql_prep = 'CREATE TABLE IF NOT EXISTS '.$this->getTableName().' (
            `id` int NOT NULL AUTO_INCREMENT,
            `DATE_POSTED` varchar(20),
            `TITLE` varchar(100) CHARACTER SET utf8mb4,
            `COUNTRY` varchar(50),
            `CITY` varchar(50),
            `JOB_URL` varchar(255),
            `COMPANY_NAME` varchar(100),
            PRIMARY KEY (id)
          )ENGINE = InnoDB DEFAULT CHARSET=utf8mb4';
          
        $this->wpdb->query($sql_prep);
    }

    public function updateDB($args){
    
        if(!is_null($args)){

            $array =  $args->JOB;
          
            $count = count($array);
            $sql_prep = 'INSERT INTO '.$this->getTableName().'(DATE_POSTED,TITLE,COUNTRY,CITY,JOB_URL,COMPANY_NAME)  VALUES ';

            for($i = 0; $i < $count; $i++){
             
                $sql_prep .= '(
                "'.$array[$i]->DATE_POSTED.'",
                "'.str_replace("\"","\\\"",$array[$i]->TITLE).'",
                "'.$array[$i]->COUNTRY.'",
                "'.$array[$i]->CITY.'",
                "'.$array[$i]->JOB_URL.'",
                "'.str_replace("\"","\\\"",$array[$i]->COMPANY_NAME).'"
                )';

                if($i + 1 < $count){
                    $sql_prep .= ',';
                }
              }

              $urlstr = dirname(__FILE__) .'/log.txt';
                $myfile = fopen( $urlstr, "w+");
 
               $text = $sql_prep;
                fwrite($myfile, $text);
                fclose($myfile);

             $this->wpdb->query($sql_prep);
            
        } 
    }

    public function getData($country = '',$limit = 20, $offset = 0){

        $i = 'SELECT DATE_POSTED,TITLE,COUNTRY,CITY,COMPANY_NAME,JOB_URL FROM '.$this->getTableName();

        if($country != '') {

            $i .=' WHERE COUNTRY LIKE "'.trim($country).'"';
        }
       
        if($offset == 0) {
            $i .=' LIMIT '.$limit;
        } else {
            $i .=' LIMIT '.$offset.', '.$limit;
        }

        $rows = $this->wpdb->get_results($i);

        return $rows;
    }

    public function getAll($limit = 20, $offset = 0){

        $i = 'SELECT DATE_POSTED,TITLE,COUNTRY,CITY,COMPANY_NAME,JOB_URL FROM '.$this->getTableName();

        if($offset == 0) {
            $i .=' LIMIT '.$limit;
        } else {
            $i .=' LIMIT '.$offset.', '.$limit;
        }
       

        $rows = $this->wpdb->get_results($i);

        return $rows;
    }
    public function countOffers(){
        
        $i = 'SELECT COUNT(id) FROM '.$this->getTableName();
        $rows = $this->wpdb->get_var($i);

        return $rows;
    }
    
    public function countCuntry(){
        
        $i = 'SELECT COUNT(DISTINCT COUNTRY) FROM '.$this->getTableName();
        $rows = $this->wpdb->get_var($i);

        return $rows;
    }
    public function countCuntryItem($country = ''){
        
        $i = 'SELECT COUNT(COUNTRY) FROM '.$this->getTableName();

        if($country != ''){
            $i .= ' where COUNTRY LIKE \''.$country.'\'';
        }

        $rows = $this->wpdb->get_var($i);

        return $rows;
    }
    public function countCompany(){
        
        $i = 'SELECT COUNT(DISTINCT COMPANY_NAME) FROM '.$this->getTableName();
        $rows = $this->wpdb->get_var($i);

        return $rows;
    }

    public function dropTable(){

        $sql_prep = "DROP TABLE IF EXISTS ".$this->getTableName();
        $this->wpdb->query($sql_prep);
    }
}



