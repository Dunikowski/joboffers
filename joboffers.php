<?php

/*
Plugin Name: Job offers

Description: Retrieves job offers from the  https://pl.europa.jobs
Version: 1.0.1
Author: D. Dunikowski (AdVist)

*/
require_once 'libs/JobOfeers_Model.php';

class JobOffers {
    
    private static $url_source_xml = 'https://pl.europa.jobs/export/pracujwuniiorg.xml';
   
    private static $plugin_id = 'job-offers';
    private static $plugin_version = '1.0.0';
    private $model;

    function __construct(){

        $this->model = new JobOffers_Model();
        register_activation_hook(__FILE__, array($this,'onActivate'));
        add_shortcode(self::$plugin_id, array($this,'jobOffers'));
        add_shortcode(self::$plugin_id.'-count', array($this,'jobOffersCount'));
        add_shortcode(self::$plugin_id.'-country', array($this,'jobOffersCountry'));
        add_action( 'init', array($this,'addScripts') );
    }

    public function onActivate(){

        $ver_opt = self::$plugin_id.'-version';
        $instaled_version = get_option($ver_opt,NULL);

        if($instaled_version == NULL){

            $this->model->createDBTable();
          
           
            $xml_file = $this->getXML();

            if($xml_file !== false){
                $this->model->updateDB($xml_file);
            }

            update_option($ver_opt,self::$plugin_version);
        } 
    }


    public function getXML() {
    
        //$k = plugins_url('pracujwuniiorg.xml', __FILE__);
        $i = simplexml_load_file(self::$url_source_xml,"SimpleXMLElement",LIBXML_NOCDATA);

        return $i;
    }

    public function addScripts(){
        wp_register_script('job-offers-script', plugins_url('/js/job_offers.js', __FILE__),false,true);
        wp_localize_script( 'job-offers-script', 'job_offers_ajax', array( 'ajaxurl' => plugins_url( '/libs/job_offers_ajax.php', __FILE__)));
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'job-offers-script', true);
    }

    public function jobOffers($atts) {
       
        $args = shortcode_atts( 
            array(
                'country'   => '',
                'limit' => '20'
            ), 
            $atts
        );
          $country = $args['country'];
          $limit =(int) $args['limit'];
          
        $items = '';
       
        if($country != ''){

            $offer_job = $this->model->getData($country, $limit);
            $max_offer = $this->model->countCuntryItem( $country);
    
           } else {
    
            $offer_job = $this->model->getAll($limit);
    
           }
           if($offer_job){
            $items .= '<div class="loader"><img src="'.plugins_url( 'image/giphy.gif', __FILE__ ).'"></div>';
            foreach($offer_job as $item_job){
            
                $items .= '<div class="item">';
                $items .= '<div class="w-flag">';
                $items .= '<span class="'.strtolower($item_job->COUNTRY).'"></span>';
                $items .= '</div>';
                $items .=  '<a href="'.$item_job->JOB_URL.'" target="_blank">'.$item_job->TITLE.' - '.$item_job->COUNTRY.'</a>';
                $items .= '<p class="w-content-bottom">';
                $items .=  $item_job->COUNTRY.' - '.$item_job->CITY.', '.$item_job->COMPANY_NAME.', '.explode(" ",$item_job->DATE_POSTED)[0];
                $items .= '</p>';
                $items .=  '<a href="'.$item_job->JOB_URL.'" class="cta-arrow" target="_blank">Szczegóły oferty</a>';
                $items .=  '</div>';
            }
        }
       
        return '<div class="w-job-offers js-job-offers" data-country="'.$country.'"  data-max-offers="'.$max_offer.'">'.$items.'</div>'; 
    }
    public function jobOffersCount($atts){
  
        $args = shortcode_atts( 
            array(
                'option'   => ''
            ), 
            $atts
        );
          $option = $args['option'];
          switch ($option) {
            case "offers":
                $i = $this->model->countOffers();
                break;
            case "country":
                $i = $this->model->countCuntry();
                break;
            case "company":
                $i = $this->model->countCompany();
                break;
            default:
            $i = $this->model->countOffers();
            $option = 'offers';
        }
          

       
        return '<span class="count-'.$option.'">'.$i.'</span>';
    }
    public function jobOffersCountry($atts){
        $args = shortcode_atts( 
            array(
                'option'   => 'niemcy'
            ), 
            $atts
        );
        $i = $args['option'];
        $offer_job_count = $this->model->countCuntryItem($i);
        return '<span class="count-country">'.$offer_job_count.'</span>';
    }
}

$JobOffers= new JobOffers();



