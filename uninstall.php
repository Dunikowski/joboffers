<?php
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}

delete_option('job-offers-version');
remove_shortcode('job-offers');
remove_shortcode('job-offers-count');
remove_shortcode('job-offers-country');

global $wpdb;
$sql_prep = "DROP TABLE IF EXISTS wp_job_offers";
$wpdb->query($sql_prep);

