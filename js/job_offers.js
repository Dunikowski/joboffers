(function($){
    $(document).ready(function(){

        let JobOffers = {};
        JobOffers.container = $('.js-job-offers');
        JobOffers.isLoad = false;
        JobOffers.ajax = function(){
           const _country = JobOffers.container.data("country") || '';
           const _offset = JobOffers.container.find('.item').length;
           const _limit = JobOffers.container.data("limit") || 20;
           const _loader = JobOffers.container.find('>.loader');
           const _max_offers = JobOffers.container.data("max-offers");
           if(_offset >=_max_offers ) return;
           _loader.addClass('show');
            $.ajax({
                url: job_offers_ajax.ajaxurl,
                type: 'post',
                data: {
                    country: _country,
                    offset: _offset,
                    limit: _limit
                },
                success: function( response ) {
                   
                    let i = '';
                  
                    const _res = JSON.parse(response);
                    
                    _res.forEach(element => {
                        i += `<div class="item">
                            <div class="w-flag">
                                <span class="${element.COUNTRY.toLowerCase()}"></span>
                            </div>
                            <a href="${element.JOB_URL}" target="_blank">${element.TITLE} - ${element.COUNTRY}</a>
                            <p class="w-content-bottom">
                            ${element.COUNTRY} - ${element.CITY}, ${element.COMPANY_NAME},  ${element.DATE_POSTED.split(' ')[0]}
                            </p>
                            <a href="${element.JOB_URL}" class="cta-arrow" target="_blank">Szczegóły oferty</a>
                        </div>`;
                        
                   });
                  
                JobOffers.container.append($(i));
                JobOffers.isLoad = false;
                _loader.removeClass('show');
                },
            });
        }

        if(JobOffers.container.length > 0){
            $(window).on('scroll', function() {
           
                if (!JobOffers.isLoad && JobOffers.container.offset().top + JobOffers.container.outerHeight(true) <= $(window).scrollTop() +  $(window).height()) {
                    JobOffers.isLoad = true;
                    JobOffers.ajax();
                }
                return;
              });
           
        } 

    });
    
})(jQuery)